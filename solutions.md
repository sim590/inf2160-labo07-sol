# INF2160 - Labo07 - Solutions


## 1 - Entrées et sorties

> Écrivez un module Haskell appelé `TrimTrailing.hs` qui lit sur `stdin` une
> suite de lignes, qui supprime les espaces en fin de ligne (en anglais, on les
> appelle *trailing spaces*) et qui affiche le résultat sur `stdout`.

Deux solutions ont étées proposées en classe par Lilly et Arnaud. La première:

```haskell
import Data.Char

main = interact noSpaceLines

noSpaceLines :: String -> String
noSpaceLines input =
    let allLines   = lines input
        spaceLines = map (\line -> spaceRecu line) allLines
        result     = unlines spaceLines
    in  result

spaceRecu :: String -> String
spaceRecu [] = []
spaceRecu [x] = if (x == ' ') then [] else [x]
spaceRecu xs = if (last xs == ' ') then spaceRecu (init xs) else xs
```

La seconde solution:

```haskell
import Data.Traversable

main = do contents <- getContents
          forM (lines contents) $ \aline -> do trimTrailing aline

trimTrailing :: [Char] -> IO ()
trimTrailing [] = putStrLn ""
trimTrailing s | last s == ' ' = trimTrailing $ init s
               | otherwise = putStrLn s
```

L'avantage de la première est l'utilisation plus adéquate de la fonction
[`interact`][interact]. Celle-ci permet de prendre l'entièreté de l'entrée
standard et de la passer à la fonction qu'on désire. Cette dernière renvoie un
type `String` qui est écrit à la sortie standard. Comme le programme à faire
demande exactement à faire cela, il s'agit de la méthode la plus adéquate.
Cependant, la seconde solution semble plus courte. On peut en effet raccourcir
la première afin d'obtenir un mélange des deux solutions comme suit:

```haskell
main = interact (unlines . map trimTrailing . lines)

trimTrailing :: String -> String
trimTrailing [] = ""
trimTrailing s | last s == ' ' = trimTrailing $ init s
               | otherwise = s
```

[interact]: https://hackage.haskell.org/package/base-4.8.2.0/docs/Prelude.html#v:interact

## 2 - Récursivité

> Proposez une implémentation (récursive) des fonctions suivantes (sans faire
> appel à d'autres fonctions, comme map, filter, etc.):

```haskell
-- "Zippe" deux listes
zip' :: [a] -> [b] -> [(a,b)]
zip' as [] = []
zip' [] bs = []
zip' (a:as) (b:bs) = (a, b) : zip' as bs
```

```haskell
-- "Zippe" deux listes en appliquant une fonction
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' f as [] = []
zipWith' f [] bs = []
zipWith' f (a:as) (b:bs) (f a b) : zipWith' f as bs
```

```haskell
-- Indique si la première liste est préfixe de la deuxième
isPrefixOf' :: Eq a => [a] -> [a] -> Bool
isPrefixOf' (a:as) (b:bs)
        | length (a:as) > length (b:bs) = False
        | length (a:as) == 0 = True
        | otherwise  = a == b && isPrefixOf' as bs
```

## 3 - La fonction `iterate`

## 3.1 - La fonction `iterateWhile`

> Donnez l'implémentation d'une fonction
> ```haskell
> iterateWhile :: (a -> a -> Bool) -> (a -> a) -> a -> [a]
> ```
> qui itère une fonction sur un élément jusqu'à ce qu'une certaine condition soit
> satisfaite.

```haskell
iterateWhile :: (a -> a -> Bool) -> (a -> a) -> a -> [a]
iterateWhile f g x
    | f x (g x) = x : iterateWhile f g (g x)
    | otherwise = [x]
```

## 3.2 - La fonction `iterateUntilEqual`

> En utilisant la fonction `iterateWhile` définie plus haut, proposez une fonction
> ```haskell
> iterateUntilEqual :: Eq a => (a -> a) -> a -> [a]
> ```
> qui a le même comportement que `iterate`, mais qui stoppe l'itération
> lorsqu'une valeur est égale à la suivante dans le résultat.

```haskell
iterateUntilEqual :: Eq a => (a -> a) -> a -> [a]
iterateUntilEqual = iterateWhile (/=)
```

## 3.3 - La fonction `ancesters`

> Récupérez le module [`Simpsons.hs`](Simpsons.hs) disponible dans ce dépôt.
>
> En faisant appel à la fonction `iterateUntilEqual` définie précédemment,
> complétez l'implémentation des fonctions `parents'` et `ancesters`. En
> particulier, on s'attend aux résultats suivants:
> ```haskell
> >>> parents' ["Lisa", "Homer"]
> ["Homer","Marge","Abraham","Mona"]
> >>> ancesters "Lisa"
> ["Homer","Marge","Abraham","Mona","Clancy","Jackie","Orville","Yuma","Willard","Theodora"]
> ```
>
> *Note*: Il est possible de définir la fonction `ancesters` sans faire appel à
> la fonction `iterateUntilEqual`, mais il s'agit d'un excellent exercice pour
> voir si vous comprenez bien comment utiliser les fonctions d'ordre supérieur.

```haskell
type Person = String

parents = Map.fromList [("Bart",    ["Homer",   "Marge"]),
                        ("Lisa",    ["Homer",   "Marge"]),
                        ("Maggie",  ["Homer",   "Marge"]),
                        ("Marge",   ["Clancy",  "Jackie"]),
                        ("Homer",   ["Abraham", "Mona"]),
                        ("Herb",    ["Abraham"]),
                        ("Patty",   ["Clancy",  "Jackie"]),
                        ("Selma",   ["Clancy",  "Jackie"]),
                        ("Abraham", ["Orville", "Yuma"]),
                        ("Yuma",    ["Willard", "Theodora"])]

parents' :: [Person] -> [Person]
parents' []     = []
parents' (x:xs) = parents'' x ++ parents' xs
    where parents'' y = case Map.lookup y parents of
              Nothing -> []
              Just ps -> ps

ancesters :: Person -> [Person]
ancesters x = (tail . concat . iterateUntilEqual parents') [x]
```

<!-- vim: set ts=4 sw=4 tw=80 et :-->

